(* fuse.ml: interface for libfuse-ocaml
 * Copyright (C) 2009 Goswin von Brederlow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Under Debian a copy can be found in /usr/share/common-licenses/LGPL-3.
 *)

type error =
    SUCCESS
  | ENOENT
  | EIO
  | ENOTDIR
  | EISDIR
  | EPERM
  | ENOTEMPTY
  | ENOSPC

exception Error of error

type fuse_req
type inode = int64
type pid_t = int

module Buf = struct
  type t = (int, Bigarray.int8_unsigned_elt, Bigarray.c_layout) Bigarray.Array1.t
  external unsafe_substr : t -> int -> int -> string = "caml_fuse_buf_unsafe_substr_stub"
  let substr buf off len =
    let dim = Bigarray.Array1.dim buf
    in
      if dim < off
      then raise (Invalid_argument "Index out of bound.");
      if dim - off < len
      then raise (Invalid_argument "Index out of bound.");
      unsafe_substr buf off len
  let str buf =
    unsafe_substr buf 0 (Bigarray.Array1.dim buf)
end

type stats = {
  st_dev : int;
  st_ino : int64;
  st_mode : int;
  st_nlink : int;
  st_uid : int;
  st_gid : int;
  st_rdev : int;
  st_size : int64;
  st_blksize : int64;
  st_blocks : int64;
  st_atime : float;
  st_mtime : float;
  st_ctime : float;
}
type entry = {
  e_inode : inode;
  e_generation : int64;
  e_stats : stats;
  e_stats_timeout : float;
  e_entry_timeout : float;
}
type open_flags = int
type 'a file_info = {
  fi_flags : open_flags;
  fi_writepage : bool;
  fi_direct_io : bool;
  fi_keep_cache : bool;
  fi_flush : bool;
  fi_fh : 'a option;
  fi_lock_owner : int64;
}
type context = {
  ctx_uid : int;
  ctx_gid : int;
  ctx_pid : pid_t;
}
type set_attr_flags = {
  set_attr_mode : bool;
  set_attr_uid : bool;
  set_attr_gid : bool;
  set_attr_size : bool;
  set_attr_atime : bool;
  set_attr_mtime : bool;
  set_attr_fh : bool;
  set_attr_atime_now : bool;
  set_attr_mtime_now : bool;
  set_attr_lockowner : bool;
}
type mode = int
type dev = int
type statfs = {
  f_bsize : int64;
  f_frsize : int64;
  f_blocks : int64;
  f_bfree : int64;
  f_bavail : int64;
  f_files : int64;
  f_ffree : int64;
  f_favail : int64;
  f_fsid : int64;
  f_flag : int64;
  f_namemax : int64;
}
type flock = {
  l_type : int;
  l_whence : int;
  l_start : int64;
  l_len : int64;
  l_pid : pid_t;
}

external fuse_reply_err : fuse_req -> error -> unit = "caml_fuse_reply_err_stub"
external fuse_reply_none : fuse_req -> unit = "caml_fuse_reply_none_stub"
external fuse_reply_entry : fuse_req -> entry -> unit = "caml_fuse_reply_entry_stub"
external fuse_reply_create : fuse_req -> entry -> 'a file_info -> unit = "caml_fuse_reply_create_stub"
external fuse_reply_attr : fuse_req -> stats -> float -> unit = "caml_fuse_reply_attr_stub"
external fuse_reply_readlink : fuse_req -> string -> unit = "caml_fuse_reply_readlink_stub"
external fuse_reply_open : fuse_req -> 'a file_info -> unit = "caml_fuse_reply_open_stub"
external fuse_reply_write : fuse_req -> int -> unit = "caml_fuse_reply_write_stub"
external fuse_reply_buf : fuse_req -> Buf.t -> unit = "caml_fuse_reply_buf_stub"
external fuse_reply_iov : fuse_req -> Buf.t array -> unit = "caml_fuse_reply_iov_stub"
external fuse_reply_strbuf : fuse_req -> string -> unit = "caml_fuse_reply_strbuf_stub"
external fuse_reply_striov : fuse_req -> string array -> unit = "caml_fuse_reply_striov_stub"
external fuse_reply_statfs : fuse_req -> statfs -> unit = "caml_fuse_reply_statfs_stub"
external fuse_reply_xattr : fuse_req -> int -> unit = "caml_fuse_reply_xattr_stub"
external fuse_reply_lock : fuse_req -> flock -> unit = "caml_fuse_reply_lock_stub"
external fuse_reply_bmap : fuse_req -> int64 -> unit = "caml_fuse_reply_bmap_stub"

type 'a reply =
    [
    | `Err of error
    | `None
    | `Entry of entry
    | `Create of entry * 'a file_info
    | `Attr of stats * float
    | `Readlink of string
    | `Open of 'a file_info
    | `Write of int
    | `Buf of Buf.t
    | `StrBuf of string
    | `Iov of Buf.t array
    | `StrIov of string array
    | `Statfs of statfs
    | `Xattr of int
    | `Lock of flock
    | `Bmap of int64
    ]

let reply req = function
    `Err err -> fuse_reply_err req err
  | `None -> fuse_reply_none req
  | `Entry entry -> fuse_reply_entry req entry
  | `Create (entry, file_info) -> fuse_reply_create req entry file_info
  | `Attr (stats, timeout) -> fuse_reply_attr req stats timeout
  | `Readlink str -> fuse_reply_readlink req str
  | `Open file_info -> fuse_reply_open req file_info
  | `Write size -> fuse_reply_write req size
  | `Buf buf -> fuse_reply_buf req buf
  | `StrBuf str -> fuse_reply_strbuf req str
  | `Iov buf_array -> fuse_reply_iov req buf_array
  | `StrIov str_array -> fuse_reply_striov req str_array
  | `Statfs statfs -> fuse_reply_statfs req statfs
  | `Xattr size -> fuse_reply_xattr req size
  | `Lock flock -> fuse_reply_lock req flock
  | `Bmap idx -> fuse_reply_bmap req idx

type lookup_res      = [`Entry of entry | `Err of error]
let reply_lookup req (res : lookup_res) = reply req res

type forget_res      = [`None]
let reply_forget req (res : forget_res) = reply req res

type getattr_res     = [`Attr of stats * float | `Err of error]
let reply_getattr req (res : getattr_res) = reply req res

type setattr_res     = [`Attr of stats * float | `Err of error]
let reply_setattr req (res : setattr_res) = reply req res

type readlink_res    = [`Readlink of string | `Err of error]
let reply_readlink req (res : readlink_res) = reply req res

type mknod_res       = [`Entry of entry | `Err of error]
let reply_mknod req (res : mknod_res) = reply req res

type mkdir_res       = [`Entry of entry | `Err of error]
let reply_mkdir req (res : mkdir_res) = reply req res

type unlink_res      = [`Err of error]
let reply_unlink req (res : unlink_res) = reply req res

type rmdir_res       = [`Err of error]
let reply_rmdir req (res : rmdir_res) = reply req res

type symlink_res     = [`Entry of entry | `Err of error]
let reply_symlink req (res : symlink_res) = reply req res

type rename_res      = [`Err of error]
let reply_rename req (res : rename_res) = reply req res

type link_res        = [`Entry of entry | `Err of error]
let reply_link req (res : link_res) = reply req res

type 'a open_res     = [`Open of 'a file_info | `Err of error]
let reply_open req (res : 'a open_res) = reply req res

type read_res        = [`Buf of Buf.t | `StrBuf of string | `Iov of Buf.t array | `StrIov of string array | `Err of error]
let reply_read req (res : read_res) = reply req res

type write_res       = [`Write of int | `Err of error]
let reply_write req (res : write_res) = reply req res

type flush_res       = [`Err of error]
let reply_flush req (res : flush_res) = reply req res

type release_res     = [`Err of error]
let reply_release req (res : release_res) = reply req res

type fsync_res       = [`Err of error]
let reply_fsync req (res : fsync_res) = reply req res

type 'a opendir_res  = [`Open of 'a file_info | `Err of error]
let reply_opendir req (res : 'a opendir_res) = reply req res

type readdir_res     = [`Buf of Buf.t | `StrBuf of string | `Iov of Buf.t array | `StrIov of string array | `Err of error]
let reply_readdir req (res : readdir_res) = reply req res

type releasedir_res  = [`Err of error]
let reply_releasedir req (res : releasedir_res) = reply req res

type fsyncdir_res    = [`Err of error]
let reply_fsyncdir req (res : fsyncdir_res) = reply req res

type statfs_res      = [`Statfs of statfs | `Err of error]
let reply_statfs req (res : statfs_res) = reply req res

type setxattr_res    = [`Err of error]
let reply_setxattr req (res : setxattr_res) = reply req res

type getxattr_res    =
    [`Buf of Buf.t | `StrBuf of string | `Iov of Buf.t array | `StrIov of string array | `Xattr of int | `Err of error]
let reply_getxattr req (res : getxattr_res) = reply req res

type listxattr_res   =
    [`Buf of Buf.t | `StrBuf of string | `Iov of Buf.t array | `StrIov of string array | `Xattr of int | `Err of error]
let reply_listxattr req (res : listxattr_res) = reply req res

type removexattr_res = [`Err of error]
let reply_removexattr req (res : removexattr_res) = reply req res

type access_res      = [`Err of error]
let reply_access req (res : access_res) = reply req res

type 'a create_res   = [`Create of entry * 'a file_info | `Err of error]
let reply_create req (res : 'a create_res) = reply req res

type getlk_res       = [`Lock of flock | `Err of error]
let reply_getlk req (res : getlk_res) = reply req res

type setlk_res       = [`Err of error]
let reply_setlk req (res : setlk_res) = reply req res

type bmap_res        = [`Bmap of int64 | `Err of error]
let reply_bmap req (res : bmap_res) = reply req res


type 'a ops = {
  init        : unit -> unit;
  destroy     : unit -> unit;
  lookup      : fuse_req -> inode -> string -> unit;
  forget      : fuse_req -> inode -> int64 -> unit;
  getattr     : fuse_req -> inode -> unit;
  setattr     : fuse_req -> inode -> stats -> set_attr_flags -> 'a file_info option -> unit;
  readlink    : fuse_req -> inode -> unit;
  mknod       : fuse_req -> inode -> string -> mode -> dev -> unit;
  mkdir       : fuse_req -> inode -> string -> mode -> unit;
  unlink      : fuse_req -> inode -> string -> unit;
  rmdir       : fuse_req -> inode -> string -> unit;
  symlink     : fuse_req -> string -> inode -> string -> unit;
  rename      : fuse_req -> inode -> string -> inode -> string -> unit;
  link        : fuse_req -> inode -> inode -> string -> unit;
  openfile    : fuse_req -> inode -> 'a file_info -> unit;
  read        : fuse_req -> inode -> int -> int64 -> 'a file_info -> unit;
  write       : fuse_req -> inode -> Buf.t -> int64 -> 'a file_info -> unit;
  flush       : fuse_req -> inode -> 'a file_info -> unit;
  release     : fuse_req -> inode -> 'a file_info -> unit;
  fsync       : fuse_req -> inode -> bool -> 'a file_info -> unit;
  opendir     : fuse_req -> inode -> 'a file_info -> unit;
  readdir     : fuse_req -> inode -> int -> int64 -> 'a file_info -> unit;
  releasedir  : fuse_req -> inode -> 'a file_info -> unit;
  fsyncdir    : fuse_req -> inode -> bool -> 'a file_info -> unit;
  statfs      : fuse_req -> inode -> unit;
  setxattr    : fuse_req -> inode -> string -> string -> int -> unit;
  getxattr    : fuse_req -> inode -> string -> int -> unit;
  listxattr   : fuse_req -> inode -> int -> unit;
  removexattr : fuse_req -> inode -> string -> unit;
  access      : fuse_req -> inode -> int -> unit;
  create      : fuse_req -> inode -> string -> mode -> 'a file_info -> unit;
  getlk       : fuse_req -> inode -> 'a file_info -> flock -> unit;
  setlk       : fuse_req -> inode -> 'a file_info -> flock -> bool -> unit;
  bmap        : fuse_req -> inode -> int64 -> int64 -> unit;
}

let default_ops () = {
  init        = Obj.magic 0;
  destroy     = Obj.magic 0;
  lookup      = Obj.magic 0;
  forget      = Obj.magic 0;
  getattr     = Obj.magic 0;
  setattr     = Obj.magic 0;
  readlink    = Obj.magic 0;
  mknod       = Obj.magic 0;
  mkdir       = Obj.magic 0;
  unlink      = Obj.magic 0;
  rmdir       = Obj.magic 0;
  symlink     = Obj.magic 0;
  rename      = Obj.magic 0;
  link        = Obj.magic 0;
  openfile    = Obj.magic 0;
  read        = Obj.magic 0;
  write       = Obj.magic 0;
  flush       = Obj.magic 0;
  release     = Obj.magic 0;
  fsync       = Obj.magic 0;
  opendir     = Obj.magic 0;
  readdir     = Obj.magic 0;
  releasedir  = Obj.magic 0;
  fsyncdir    = Obj.magic 0;
  statfs      = Obj.magic 0;
  setxattr    = Obj.magic 0;
  getxattr    = Obj.magic 0;
  listxattr   = Obj.magic 0;
  removexattr = Obj.magic 0;
  access      = Obj.magic 0;
  create      = Obj.magic 0;
  getlk       = Obj.magic 0;
  setlk       = Obj.magic 0;
  bmap        = Obj.magic 0;
}

let root_inode = Int64.one
let s_IFDIR  = 0o040000
let s_IFREG  = 0o100000
let s_IFMASK = 0o770000

let s_ISDIR mode = (mode land s_IFMASK) = s_IFDIR
let s_ISREG mode = (mode land s_IFMASK) = s_IFREG

type filesystem
external mount : string -> string list -> 'a ops -> filesystem = "caml_fuse_mount_stub"
external umount : filesystem -> unit = "caml_fuse_umount_stub"
external fd : filesystem -> Unix.file_descr = "caml_fuse_fd_stub"
external session_exited : filesystem -> bool = "caml_fuse_session_exited"
external process : filesystem -> unit = "caml_fuse_process_stub"

external add_direntry : fuse_req -> string -> int -> (string * stats * int64) -> int = "caml_fuse_add_direntry_stub"
    (* req buf pos (name, stats, offset) *)

external context : fuse_req -> context = "caml_fuse_context_stub"

let make mountpoint args ops = mount mountpoint args ops
