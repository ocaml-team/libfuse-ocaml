/* fuse_stubs.c: interface stubs for libfuse-ocaml
 * Copyright (C) 2009 Goswin von Brederlow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Under Debian a copy can be found in /usr/share/common-licenses/LGPL-3.
 */

#define FUSE_USE_VERSION 26
#include <fuse_lowlevel.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <string.h>

#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/callback.h>
#include <caml/signals.h>
#include <caml/custom.h>
#include <caml/bigarray.h>

// Lacking in <caml/bigarray.h>
CAMLprim value caml_ba_sub(value vb, value vofs, value vlen);

// Lacking in <fuse_lowlevel.h>
#define HEADER_SIZE 80

//FIXME: use sysconf
#define PAGE_SIZE 4096

// Fix stupid compatbility defines
#undef flush

enum Callbacks {
    INIT, DESTROY, LOOKUP, FORGET, GETATTR, SETATTR, READLINK, MKNOD, MKDIR,
    UNLINK, RMDIR, SYMLINK, RENAME, LINK, OPENFILE, READ, WRITE,
    FLUSH, RELEASE, FSYNC, OPENDIR, READDIR, RELEASEDIR, FSYNCDIR, STATFS,
    SETXATTR, GETXATTR, LISTXATTR, REMOVEXATTR, ACCESS, CREATE, GETLK, SETLK,
    BMAP
};

int errors[] = {0, ENOENT, EIO, ENOTDIR, EISDIR, EPERM, ENOTEMPTY, ENOSPC};

char *buffer = NULL;
char *buffer2 = NULL;
size_t buffer_size = 0;

typedef struct Filesystem {
    struct fuse_lowlevel_ops fuse_ops;
    struct fuse_chan *ch;
    struct fuse_session *se;
    value ops;
    char *mountpoint;
} Filesystem;

typedef struct File_Info_Ref {
    value file_info;
} File_Info_Ref;

#define Val_none Val_int(0)

static inline value Val_some(value v) {   
    CAMLparam1(v);
    CAMLlocal1(some);
    some = caml_alloc(1, 0);
    Store_field(some, 0, v);
    CAMLreturn(some);
}

// Convert Fuse.stats to struct stat
static void stats_ml_to_c(value ml_stat, struct stat *stat) {
    CAMLparam1(ml_stat);
    memset(stat, 0, sizeof(*stat));
    stat->st_dev     = Int_val(Field(ml_stat, 0));
    stat->st_ino     = Int64_val(Field(ml_stat, 1));
    stat->st_mode    = Int_val(Field(ml_stat, 2));
    stat->st_nlink   = Int_val(Field(ml_stat, 3));
    stat->st_uid     = Int_val(Field(ml_stat, 4));
    stat->st_gid     = Int_val(Field(ml_stat, 5));
    stat->st_rdev    = Int_val(Field(ml_stat, 6));
    stat->st_size    = Int64_val(Field(ml_stat, 7)); 
    stat->st_blksize = Int64_val(Field(ml_stat, 8));
    stat->st_blocks  = Int64_val(Field(ml_stat, 9));
    stat->st_atime   = Double_val(Field(ml_stat, 10));
    stat->st_mtime   = Double_val(Field(ml_stat, 11));
    stat->st_ctime   = Double_val(Field(ml_stat, 12));
    CAMLreturn0;
}

// Allocate a Fuse.stat and fill it in
static CAMLprim value copy_stats_c_to_ml(struct stat *stat) {
    CAMLparam0();
    CAMLlocal1(ml_stat);
    ml_stat = caml_alloc(13, 0);
    
    Store_field(ml_stat,  0, stat->st_dev);
    Store_field(ml_stat,  1, caml_copy_int64(stat->st_ino));
    Store_field(ml_stat,  2, stat->st_mode);
    Store_field(ml_stat,  3, stat->st_nlink);
    Store_field(ml_stat,  4, stat->st_uid);
    Store_field(ml_stat,  5, stat->st_gid);
    Store_field(ml_stat,  6, stat->st_rdev);
    Store_field(ml_stat,  7, caml_copy_int64(stat->st_size));
    Store_field(ml_stat,  8, caml_copy_int64(stat->st_blksize));
    Store_field(ml_stat,  9, caml_copy_int64(stat->st_blocks));
    Store_field(ml_stat, 10, caml_copy_double(stat->st_atime));
    Store_field(ml_stat, 11, caml_copy_double(stat->st_mtime));
    Store_field(ml_stat, 12, caml_copy_double(stat->st_ctime));

    CAMLreturn(ml_stat);
}

// Allocate a Fuse.set_attr_flags and fill it in
static CAMLprim value copy_set_attr_flags_c_to_ml(int to_set) {
    CAMLparam0();
    CAMLlocal1(ml_set);
    ml_set = caml_alloc(9, 0);
    
    Store_field(ml_set, 0, Val_int(to_set & (1 << 0)));
    Store_field(ml_set, 1, Val_int(to_set & (1 << 1)));
    Store_field(ml_set, 2, Val_int(to_set & (1 << 2)));
    Store_field(ml_set, 3, Val_int(to_set & (1 << 3)));
    Store_field(ml_set, 4, Val_int(to_set & (1 << 4)));
    Store_field(ml_set, 5, Val_int(to_set & (1 << 5)));
    Store_field(ml_set, 6, Val_int(to_set & (1 << 6)));
    Store_field(ml_set, 7, Val_int(to_set & (1 << 7)));
    Store_field(ml_set, 8, Val_int(to_set & (1 << 8)));
    Store_field(ml_set, 9, Val_int(to_set & (1 << 9)));

    CAMLreturn(ml_set);
}

// Allocate a Fuse.file_info and fill it in
static CAMLprim value copy_file_info_c_to_ml(struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocal1(ml_fi);
    ml_fi = caml_alloc(7, 0);
    
    Store_field(ml_fi,  0, Val_int(fi->flags));
    Store_field(ml_fi,  1, Val_int(fi->writepage));
    Store_field(ml_fi,  2, Val_int(fi->direct_io));
    Store_field(ml_fi,  3, Val_int(fi->keep_cache));
    Store_field(ml_fi,  4, Val_int(fi->flush));
    if (fi->fh == 0) {
	Store_field(ml_fi,  5, Val_none);
    } else {
	Store_field(ml_fi,  5, Val_some(((File_Info_Ref*)(fi->fh))->file_info));
    }
    Store_field(ml_fi,  6, Val_int(fi->lock_owner));

    CAMLreturn(ml_fi);
}

// Allocate a Fuse.flock and fill it in
static CAMLprim value copy_flock_c_to_ml(struct flock *lock) {
    CAMLparam0();
    CAMLlocal1(ml_flock);
    ml_flock = caml_alloc(5, 0);
    
    Store_field(ml_flock,  0, Val_int(lock->l_type));
    Store_field(ml_flock,  1, Val_int(lock->l_whence));
    Store_field(ml_flock,  2, caml_copy_int64(lock->l_start));
    Store_field(ml_flock,  3, caml_copy_int64(lock->l_len));
    Store_field(ml_flock,  4, Val_int(lock->l_pid));

    CAMLreturn(ml_flock);
}

static void init_stub(void *userdata, struct fuse_conn_info *conn) {
    printf("# caml_fuse_init_stub()\n");
    (void)conn;
    Filesystem *fs = (Filesystem*)userdata;

    fprintf(stderr, "  proto_major = %d\n", conn->proto_major);
    fprintf(stderr, "  proto_minor = %d\n", conn->proto_minor);
    fprintf(stderr, "  async_read = %d\n", conn->async_read);
    fprintf(stderr, "  max_write = %d\n", conn->max_write);
    fprintf(stderr, "  max_readahead = %d\n", conn->max_readahead);

    leave_blocking_section();
    caml_callback(Field(fs->ops, INIT), Val_unit);
    enter_blocking_section();
}

static void destroy_stub(void *userdata) {
    printf("# caml_fuse_destroy_stub()\n");
    Filesystem *fs = (Filesystem*)userdata;

    leave_blocking_section();
    caml_callback(Field(fs->ops, DESTROY), Val_unit);
    enter_blocking_section();
    printf("# caml_fuse_destroy_stub() done\n");
}

static void lookup_stub2(fuse_req_t req, fuse_ino_t parent, const char *name) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_parent, ml_name);
    printf("# caml_fuse_lookup_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_parent = caml_copy_int64(parent);
    ml_name = caml_copy_string(name);

    caml_callback3(Field(fs->ops, LOOKUP), ml_req, ml_parent, ml_name);

    CAMLreturn0;
}

static void lookup_stub(fuse_req_t req, fuse_ino_t parent, const char *name) {
    leave_blocking_section();
    lookup_stub2(req, parent, name);
    enter_blocking_section();
}

static void forget_stub2(fuse_req_t req, fuse_ino_t inode, unsigned long nlookup) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_inode, ml_nlookup);
    printf("# caml_fuse_lookup_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_inode = caml_copy_int64(inode);
    ml_nlookup = caml_copy_int64(nlookup);

    caml_callback3(Field(fs->ops, FORGET), ml_req, ml_inode, ml_nlookup);

    CAMLreturn0;
}

static void forget_stub(fuse_req_t req, fuse_ino_t inode, unsigned long nlookup) {
    leave_blocking_section();
    forget_stub2(req, inode, nlookup);
    enter_blocking_section();
}

static void getattr_stub2(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocal2(ml_req, ml_inode);
    (void)fi;

    printf("# caml_fuse_getattr_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_inode = caml_copy_int64(ino);

    caml_callback2(Field(fs->ops, GETATTR), ml_req, ml_inode);

    CAMLreturn0;
}

static void getattr_stub(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi) {
    leave_blocking_section();
    getattr_stub2(req, ino, fi);
    enter_blocking_section();
}

static void setattr_stub2(fuse_req_t req, fuse_ino_t ino, struct stat *attr,
                          int to_set, struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocalN(ml_args, 5); // ml_req ml_inode ml_attr ml_to_set ml_fi_opt
    (void)fi;

    printf("# caml_fuse_setattr_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = copy_stats_c_to_ml(attr);
    ml_args[3] = copy_set_attr_flags_c_to_ml(to_set);
    if (fi == NULL) {
	ml_args[4] = Val_none;
    } else {
	ml_args[4] = Val_some(copy_file_info_c_to_ml(fi));
    }

    caml_callbackN(Field(fs->ops, SETATTR), 5, ml_args);

    CAMLreturn0;
}

static void setattr_stub(fuse_req_t req, fuse_ino_t ino, struct stat *attr,
                         int to_set, struct fuse_file_info *fi) {
    leave_blocking_section();
    setattr_stub2(req, ino, attr, to_set, fi);
    enter_blocking_section();
}

static void readlink_stub2(fuse_req_t req, fuse_ino_t ino) {
    CAMLparam0();
    CAMLlocal2(ml_req, ml_ino);

    printf("# readlink_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_ino = caml_copy_int64(ino);

    caml_callback2(Field(fs->ops, READLINK), ml_req, ml_ino);

    CAMLreturn0;
}

static void readlink_stub(fuse_req_t req, fuse_ino_t ino) {
    leave_blocking_section();
    readlink_stub2(req, ino);
    enter_blocking_section();
}

static void mknod_stub2(fuse_req_t req, fuse_ino_t parent, const char *name,
                        mode_t mode, dev_t rdev) {
    CAMLparam0();
    CAMLlocalN(ml_args, 5); // ml_req ml_parent ml_name ml_mode ml_dev
    printf("# caml_fuse_mknod_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(parent);
    ml_args[2] = caml_copy_string(name);
    ml_args[3] = Val_int(mode);
    ml_args[4] = Val_int(rdev);

    caml_callbackN(Field(fs->ops, MKNOD), 5, ml_args);

    CAMLreturn0;
}

static void mknod_stub(fuse_req_t req, fuse_ino_t parent, const char *name,
                       mode_t mode, dev_t rdev) {
    leave_blocking_section();
    mknod_stub2(req, parent, name, mode, rdev);
    enter_blocking_section();
}

static void mkdir_stub2(fuse_req_t req, fuse_ino_t parent, const char *name, mode_t mode) {
    CAMLparam0();
    CAMLlocalN(ml_args, 4); // ml_req ml_parent ml_name ml_mode

    printf("# mkdir_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(parent);
    ml_args[2] = copy_string(name);
    ml_args[3] = Val_int(mode);

    caml_callbackN(Field(fs->ops, MKDIR), 4, ml_args);

    CAMLreturn0;
}

static void mkdir_stub(fuse_req_t req, fuse_ino_t parent, const char *name, mode_t mode) {
    leave_blocking_section();
    mkdir_stub2(req, parent, name, mode);
    enter_blocking_section();
}

static void unlink_stub2(fuse_req_t req, fuse_ino_t parent, const char *name) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_parent, ml_name);

    printf("# unlink_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_parent = caml_copy_int64(parent);
    ml_name = copy_string(name);

    caml_callback3(Field(fs->ops, UNLINK), ml_req, ml_parent, ml_name);

    CAMLreturn0;
}

static void unlink_stub(fuse_req_t req, fuse_ino_t parent, const char *name) {
    leave_blocking_section();
    unlink_stub2(req, parent, name);
    enter_blocking_section();
}

static void rmdir_stub2(fuse_req_t req, fuse_ino_t parent, const char *name) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_parent, ml_name);

    printf("# rmdir_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_parent = caml_copy_int64(parent);
    ml_name = copy_string(name);

    caml_callback3(Field(fs->ops, RMDIR), ml_req, ml_parent, ml_name);

    CAMLreturn0;
}

static void rmdir_stub(fuse_req_t req, fuse_ino_t parent, const char *name) {
    leave_blocking_section();
    rmdir_stub2(req, parent, name);
    enter_blocking_section();
}

static void symlink_stub2(fuse_req_t req, const char *link, fuse_ino_t parent, const char *name) {
    CAMLparam0();
    CAMLlocalN(ml_args, 4); // ml_req ml_link, ml_parent ml_name

    printf("# symlink_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = copy_string(link);
    ml_args[2] = caml_copy_int64(parent);
    ml_args[3] = copy_string(name);

    caml_callbackN(Field(fs->ops, SYMLINK), 4, ml_args);

    CAMLreturn0;
}

static void symlink_stub(fuse_req_t req, const char *link, fuse_ino_t parent, const char *name) {
    leave_blocking_section();
    symlink_stub2(req, link, parent, name);
    enter_blocking_section();
}

static void rename_stub2(fuse_req_t req, fuse_ino_t parent, const char *name,
                         fuse_ino_t newparent, const char *newname) {
    CAMLparam0();
    CAMLlocalN(ml_args, 5); // ml_req ml_parent ml_name ml_newparent ml_newname

    printf("# rename_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(parent);
    ml_args[2] = copy_string(name);
    ml_args[3] = caml_copy_int64(newparent);
    ml_args[4] = copy_string(newname);

    caml_callbackN(Field(fs->ops, RENAME), 5, ml_args);

    CAMLreturn0;
}

static void rename_stub(fuse_req_t req, fuse_ino_t parent, const char *name,
                         fuse_ino_t newparent, const char *newname) {
    leave_blocking_section();
    rename_stub2(req, parent, name, newparent, newname);
    enter_blocking_section();
}

static void link_stub2(fuse_req_t req, fuse_ino_t ino, fuse_ino_t newparent,
		       const char *newname) {
    CAMLparam0();
    CAMLlocalN(ml_args, 4); // ml_req ml_ino ml_newparent ml_newname

    printf("# link_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = caml_copy_int64(newparent);
    ml_args[3] = copy_string(newname);

    caml_callbackN(Field(fs->ops, LINK), 4, ml_args);

    CAMLreturn0;
}

static void link_stub(fuse_req_t req, fuse_ino_t ino, fuse_ino_t newparent,
		      const char *newname) {
    leave_blocking_section();
    link_stub2(req, ino, newparent, newname);
    enter_blocking_section();
}

static void open_stub2(fuse_req_t req, fuse_ino_t ino,
		       struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_ino, ml_fi);

    printf("# open_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_ino = caml_copy_int64(ino);
    ml_fi = copy_file_info_c_to_ml(fi);

    caml_callback3(Field(fs->ops, OPENFILE), ml_req, ml_ino, ml_fi);

    CAMLreturn0;
}

static void open_stub(fuse_req_t req, fuse_ino_t ino,
		      struct fuse_file_info *fi) {
    leave_blocking_section();
    open_stub2(req, ino, fi);
    enter_blocking_section();
}

static void read_stub2(fuse_req_t req, fuse_ino_t ino, size_t size, off_t off,
                      struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocalN(ml_args, 5); // ml_req ml_inode ml_size ml_off ml_file_info
    printf("# caml_fuse_read_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = Val_int(size);
    ml_args[3] = caml_copy_int64(off);
    ml_args[4] = copy_file_info_c_to_ml(fi);

    caml_callbackN(Field(fs->ops, READ), 5, ml_args);

    CAMLreturn0;
}

static void read_stub(fuse_req_t req, fuse_ino_t ino, size_t size, off_t off,
                      struct fuse_file_info *fi) {
    leave_blocking_section();
    read_stub2(req, ino, size, off, fi);
    enter_blocking_section();
}

static void write_stub2(fuse_req_t req, fuse_ino_t ino, const char *buf,
			size_t size, off_t off, struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocal1(ml_arr);
    CAMLlocalN(ml_args, 5); // ml_req ml_inode ml_buf ml_off ml_file_info
    printf("# caml_fuse_write_buf_stub(): buffer = %p %p, buf = %p\n", buffer, buffer2, buf);

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_arr = caml_ba_alloc_dims(CAML_BA_UINT8 | CAML_BA_C_LAYOUT | CAML_BA_MANAGED, 1, buffer, buffer_size);

    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = caml_ba_sub(ml_arr, buf - buffer, size);
    ml_args[3] = caml_copy_int64(off);
    ml_args[4] = copy_file_info_c_to_ml(fi);

    buffer = NULL;

    caml_callbackN(Field(fs->ops, WRITE), 5, ml_args);

    CAMLreturn0;
}

static void write_stub(fuse_req_t req, fuse_ino_t ino, const char *buf,
                       size_t size, off_t off, struct fuse_file_info *fi) {
    leave_blocking_section();
    write_stub2(req, ino, buf, size, off, fi);
    enter_blocking_section();
}

/*
static void write_str_stub2(fuse_req_t req, fuse_ino_t ino, const char *buf,
			size_t size, off_t off, struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocalN(ml_args, 5); // ml_req ml_inode ml_buf ml_off ml_file_info
    printf("# caml_fuse_write_str_stub(): buffer = %p %p, buf = %p\n", buffer, buffer2, buf);

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);

    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = caml_alloc_string(size);
    ml_args[3] = caml_copy_int64(off);
    ml_args[4] = copy_file_info_c_to_ml(fi);

    memcpy(String_val(ml_args[2]), buf, size);

    caml_callbackN(Field(fs->ops, WRITE), 5, ml_args);

    CAMLreturn0;
}

static void write_str_stub(fuse_req_t req, fuse_ino_t ino, const char *buf,
                       size_t size, off_t off, struct fuse_file_info *fi) {
    leave_blocking_section();
    write_str_stub2(req, ino, buf, size, off, fi);
    enter_blocking_section();
}
*/

static void flush_stub2(fuse_req_t req, fuse_ino_t ino,
		       struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_ino, ml_fi);

    printf("# flush_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_ino = caml_copy_int64(ino);
    ml_fi = copy_file_info_c_to_ml(fi);

    caml_callback3(Field(fs->ops, FLUSH), ml_req, ml_ino, ml_fi);

    CAMLreturn0;
}

static void flush_stub(fuse_req_t req, fuse_ino_t ino,
		      struct fuse_file_info *fi) {
    leave_blocking_section();
    flush_stub2(req, ino, fi);
    enter_blocking_section();
}

static void release_stub2(fuse_req_t req, fuse_ino_t ino,
		       struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_ino, ml_fi);

    printf("# release_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_ino = caml_copy_int64(ino);
    ml_fi = copy_file_info_c_to_ml(fi);

    caml_callback3(Field(fs->ops, RELEASE), ml_req, ml_ino, ml_fi);

    CAMLreturn0;
}

static void release_stub(fuse_req_t req, fuse_ino_t ino,
		      struct fuse_file_info *fi) {
    leave_blocking_section();
    release_stub2(req, ino, fi);
    enter_blocking_section();
}

static void fsync_stub2(fuse_req_t req, fuse_ino_t ino, int datasync,
		       struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocalN(ml_args, 4); // ml_req ml_ino ml_datasync ml_fi

    printf("# fsync_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = Val_bool(datasync);
    ml_args[3] = copy_file_info_c_to_ml(fi);

    caml_callbackN(Field(fs->ops, FSYNC), 4, ml_args);

    CAMLreturn0;
}

static void fsync_stub(fuse_req_t req, fuse_ino_t ino, int datasync,
		       struct fuse_file_info *fi) {
    leave_blocking_section();
    fsync_stub2(req, ino, datasync, fi);
    enter_blocking_section();
}

static void opendir_stub2(fuse_req_t req, fuse_ino_t ino,
		       struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_ino, ml_fi);

    printf("# opendir_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_ino = caml_copy_int64(ino);
    ml_fi = copy_file_info_c_to_ml(fi);

    caml_callback3(Field(fs->ops, OPENDIR), ml_req, ml_ino, ml_fi);

    CAMLreturn0;
}

static void opendir_stub(fuse_req_t req, fuse_ino_t ino,
		      struct fuse_file_info *fi) {
    leave_blocking_section();
    opendir_stub2(req, ino, fi);
    enter_blocking_section();
}

static void readdir_stub2(fuse_req_t req, fuse_ino_t ino, size_t size,
			  off_t off, struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocalN(ml_args, 5); // ml_req ml_inode ml_size ml_off ml_file_info
    printf("# caml_fuse_readdir_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = Val_int(size);
    ml_args[3] = caml_copy_int64(off);
    ml_args[4] = copy_file_info_c_to_ml(fi);

    caml_callbackN(Field(fs->ops, READDIR), 5, ml_args);

    CAMLreturn0;
}

static void readdir_stub(fuse_req_t req, fuse_ino_t ino, size_t size,
			 off_t off, struct fuse_file_info *fi) {
    leave_blocking_section();
    readdir_stub2(req, ino, size, off, fi);
    enter_blocking_section();
}

static void releasedir_stub2(fuse_req_t req, fuse_ino_t ino,
		       struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_ino, ml_fi);

    printf("# releasedir_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_ino = caml_copy_int64(ino);
    ml_fi = copy_file_info_c_to_ml(fi);

    caml_callback3(Field(fs->ops, RELEASEDIR), ml_req, ml_ino, ml_fi);

    CAMLreturn0;
}

static void releasedir_stub(fuse_req_t req, fuse_ino_t ino,
		      struct fuse_file_info *fi) {
    leave_blocking_section();
    releasedir_stub2(req, ino, fi);
    enter_blocking_section();
}

static void fsyncdir_stub2(fuse_req_t req, fuse_ino_t ino, int datasync,
		       struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocalN(ml_args, 4); // ml_req ml_ino ml_datasync ml_fi

    printf("# fsyncdir_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = Val_bool(datasync);
    ml_args[3] = copy_file_info_c_to_ml(fi);

    caml_callbackN(Field(fs->ops, FSYNCDIR), 4, ml_args);

    CAMLreturn0;
}

static void fsyncdir_stub(fuse_req_t req, fuse_ino_t ino, int datasync,
		       struct fuse_file_info *fi) {
    leave_blocking_section();
    fsyncdir_stub2(req, ino, datasync, fi);
    enter_blocking_section();
}

static void statfs_stub2(fuse_req_t req, fuse_ino_t ino) {
    CAMLparam0();
    CAMLlocal2(ml_req, ml_ino);
    printf("# statfs_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_ino = caml_copy_int64(ino);

    caml_callback2(Field(fs->ops, STATFS), ml_req, ml_ino);

    CAMLreturn0;
}

static void statfs_stub(fuse_req_t req, fuse_ino_t ino) {
    leave_blocking_section();
    statfs_stub2(req, ino);
    enter_blocking_section();
}

static void setxattr_stub2(fuse_req_t req, fuse_ino_t ino, const char *name,
			   const char *val, size_t size, int flags) {
    CAMLparam0();
    CAMLlocalN(ml_args, 5); // ml_req ml_ino ml_name ml_value ml_flags

    printf("# setxattr_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = copy_string(name);
    ml_args[3] = caml_alloc_string(size);
    ml_args[4] = Val_int(flags);

    memcpy(String_val(ml_args[3]), val, size);

    caml_callbackN(Field(fs->ops, SETXATTR), 5, ml_args);

    CAMLreturn0;
}

static void setxattr_stub(fuse_req_t req, fuse_ino_t ino, const char *name,
			  const char *val, size_t size, int flags) {
    leave_blocking_section();
    setxattr_stub2(req, ino, name, val, size, flags);
    enter_blocking_section();
}

static void getxattr_stub2(fuse_req_t req, fuse_ino_t ino, const char *name,
			   size_t size) {
    CAMLparam0();
    CAMLlocalN(ml_args, 4); // ml_req ml_ino ml_name ml_size

    printf("# getxattr_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = copy_string(name);
    ml_args[3] = Val_int(size);

    caml_callbackN(Field(fs->ops, GETXATTR), 4, ml_args);

    CAMLreturn0;
}

static void getxattr_stub(fuse_req_t req, fuse_ino_t ino, const char *name,
			  size_t size) {
    leave_blocking_section();
    getxattr_stub2(req, ino, name, size);
    enter_blocking_section();
}

static void listxattr_stub2(fuse_req_t req, fuse_ino_t ino, size_t size) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_ino, ml_size);
    printf("# listxattr_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_ino = caml_copy_int64(ino);
    ml_size = Val_int(size);

    caml_callback3(Field(fs->ops, LISTXATTR), ml_req, ml_ino, ml_size);

    CAMLreturn0;
}

static void listxattr_stub(fuse_req_t req, fuse_ino_t ino, size_t size) {
    leave_blocking_section();
    listxattr_stub2(req, ino, size);
    enter_blocking_section();
}

static void removexattr_stub2(fuse_req_t req, fuse_ino_t ino, const char *name) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_ino, ml_name);
    printf("# removexattr_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_ino = caml_copy_int64(ino);
    ml_name = caml_copy_string(name);

    caml_callback3(Field(fs->ops, REMOVEXATTR), ml_req, ml_ino, ml_name);

    CAMLreturn0;
}

static void removexattr_stub(fuse_req_t req, fuse_ino_t ino, const char *name) {
    leave_blocking_section();
    removexattr_stub2(req, ino, name);
    enter_blocking_section();
}

static void access_stub2(fuse_req_t req, fuse_ino_t ino, int mask) {
    CAMLparam0();
    CAMLlocal3(ml_req, ml_ino, ml_mask);
    printf("# access_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_req = caml_alloc(1, Abstract_tag);
    Store_field(ml_req, 0, (intptr_t)req);
    ml_ino = caml_copy_int64(ino);
    ml_mask = Val_int(mask);

    caml_callback3(Field(fs->ops, ACCESS), ml_req, ml_ino, ml_mask);

    CAMLreturn0;
}

static void access_stub(fuse_req_t req, fuse_ino_t ino, int mask) {
    leave_blocking_section();
    access_stub2(req, ino, mask);
    enter_blocking_section();
}

static void create_stub2(fuse_req_t req, fuse_ino_t parent, const char *name,
			 mode_t mode, struct fuse_file_info *fi) {
    CAMLparam0();
    CAMLlocalN(ml_args, 5); // ml_req ml_parent ml_name ml_mode ml_file_info
    printf("# caml_fuse_create_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(parent);
    ml_args[2] = caml_copy_string(name);
    ml_args[3] = Val_int(mode);
    ml_args[4] = copy_file_info_c_to_ml(fi);

    caml_callbackN(Field(fs->ops, CREATE), 5, ml_args);

    CAMLreturn0;
}

static void create_stub(fuse_req_t req, fuse_ino_t parent, const char *name,
			 mode_t mode, struct fuse_file_info *fi) {
    leave_blocking_section();
    create_stub2(req, parent, name, mode, fi);
    enter_blocking_section();
}

static void getlk_stub2(fuse_req_t req, fuse_ino_t ino,
			struct fuse_file_info *fi, struct flock *lock) {
    CAMLparam0();
    CAMLlocalN(ml_args, 4); // ml_req ml_ino ml_file_info ml_flock
    printf("# caml_fuse_getlk_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = copy_file_info_c_to_ml(fi);
    ml_args[3] = copy_flock_c_to_ml(lock);

    caml_callbackN(Field(fs->ops, GETLK), 4, ml_args);

    CAMLreturn0;
}

static void getlk_stub(fuse_req_t req, fuse_ino_t ino,
			struct fuse_file_info *fi, struct flock *lock) {
    leave_blocking_section();
    getlk_stub2(req, ino, fi, lock);
    enter_blocking_section();
}

static void setlk_stub2(fuse_req_t req, fuse_ino_t ino,
			struct fuse_file_info *fi, struct flock *lock,
			int sleep) {
    CAMLparam0();
    CAMLlocalN(ml_args, 5); // ml_req ml_ino ml_file_info ml_flock ml_sleep
    printf("# caml_fuse_setlk_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = copy_file_info_c_to_ml(fi);
    ml_args[3] = copy_flock_c_to_ml(lock);
    ml_args[4] = Val_bool(sleep);

    caml_callbackN(Field(fs->ops, SETLK), 5, ml_args);

    CAMLreturn0;
}

static void setlk_stub(fuse_req_t req, fuse_ino_t ino,
		       struct fuse_file_info *fi, struct flock *lock,
		       int sleep) {
    leave_blocking_section();
    setlk_stub2(req, ino, fi, lock, sleep);
    enter_blocking_section();
}

static void bmap_stub2(fuse_req_t req, fuse_ino_t ino, size_t blocksize,
		       uint64_t idx) {
    CAMLparam0();
    CAMLlocalN(ml_args, 4); // ml_req ml_ino ml_blocksize ml_idx
    printf("# caml_fuse_bmap_stub()\n");

    Filesystem *fs = (Filesystem*)fuse_req_userdata(req);
    ml_args[0] = caml_alloc(1, Abstract_tag);
    Store_field(ml_args[0], 0, (intptr_t)req);
    ml_args[1] = caml_copy_int64(ino);
    ml_args[2] = caml_copy_int64(blocksize);
    ml_args[3] = caml_copy_int64(idx);

    caml_callbackN(Field(fs->ops, BMAP), 4, ml_args);

    CAMLreturn0;
}

static void bmap_stub(fuse_req_t req, fuse_ino_t ino, size_t blocksize,
		      uint64_t idx) {
    leave_blocking_section();
    bmap_stub2(req, ino, blocksize, idx);
    enter_blocking_section();
}

static int alive(Filesystem *fs) {
    return (fs->mountpoint != NULL);
}

static void umount_common(Filesystem *fs) {
    fprintf(stderr, "# caml_fuse_umount_common()\n");
    fflush(stderr);
    if (fs->se != NULL) fuse_remove_signal_handlers(fs->se);
    if (fs->ch != NULL) fuse_session_remove_chan(fs->ch);
    if (fs->se != NULL) fuse_session_destroy(fs->se);
    if (fs->ch != NULL && fs->mountpoint != NULL)
	fuse_unmount(fs->mountpoint, fs->ch);
    fs->se = NULL;
    fs->ch = NULL;
    fs->mountpoint = NULL;
}

CAMLprim value caml_fuse_umount_stub(value ml_fs) {
    CAMLparam1(ml_fs);
    Filesystem *fs = *(Filesystem**)Data_custom_val(ml_fs);
    fprintf(stderr, "# caml_fuse_umount_stub()\n");
    fflush(stderr);
    umount_common(fs);
    CAMLreturn(Val_unit);
}

void caml_fuse_filesystem_finalize(value v) {
    Filesystem *fs = *(Filesystem**)Data_custom_val(v);
    fprintf(stderr, "# caml_fuse_filesystem_finalize()\n");
    fflush(stderr);
    if (fs->mountpoint != NULL) umount_common(fs);
    if (fs->ops != 0) {
	caml_remove_global_root(&fs->ops);
	fs->ops = 0;
    }
    *(Filesystem**)Data_custom_val(v) = NULL;
    free(fs);
    if (buffer != NULL) {
	free(buffer);
	buffer = NULL;
    }
}

static struct custom_operations caml_fuse_filesystem_ops = {
  "vonbrederlow.de.fuse.filesystem",
  caml_fuse_filesystem_finalize,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

CAMLprim value caml_fuse_mount_stub(value ml_mountpoint, value ml_args, value ml_ops) {
    struct fuse_args fuse_args = FUSE_ARGS_INIT(0, NULL);
    CAMLparam3(ml_mountpoint, ml_args, ml_ops);
    CAMLlocal2(ml_argp, ml_fs);

    // Add args
    if (fuse_opt_add_arg(&fuse_args, String_val(ml_mountpoint))) {
	fprintf(stderr, "Adding the -o default_permissions option failed.\n");
	fflush(stderr);
	exit(1);
    }
    ml_argp = ml_args;
    while(ml_argp != Val_int(0)) {
	if (fuse_opt_add_arg(&fuse_args, String_val(Field(ml_argp, 0)))) {
	    fprintf(stderr, "Adding the %s option failed.\n", String_val(Field(ml_argp,0)));
	    fflush(stderr);
	    exit(1);
	}
	ml_argp = Field(ml_argp, 1);
    }

    // Allocate ocaml filesystem structure
    ml_fs = caml_alloc_custom(&caml_fuse_filesystem_ops, sizeof(Filesystem*), 0, 1);
    if (ml_fs == 0) goto out;

    // Allocate C filesystem structure and store it
    Filesystem *fs = malloc(sizeof(Filesystem));
    *(Filesystem**)Data_custom_val(ml_fs) = fs;
    memset(fs, 0, sizeof(Filesystem));

    // Store mountpoint
    fs->mountpoint = strdup(String_val(ml_mountpoint));
    if (fs->mountpoint == NULL) goto out;

    // Register ml_ops as root
    caml_register_global_root(&fs->ops);
    fs->ops = ml_ops;

    // Fill in fuse_ops
    if (Field(ml_ops, INIT) != Val_int(0)) {
	fs->fuse_ops.init = init_stub;
    }
    if (Field(ml_ops, DESTROY) != Val_int(0)) {
	fs->fuse_ops.destroy = destroy_stub;
    }
    if (Field(ml_ops, LOOKUP) != Val_int(0)) {
	fs->fuse_ops.lookup = lookup_stub;
    }
    if (Field(ml_ops, FORGET) != Val_int(0)) {
	fs->fuse_ops.forget = forget_stub;
    }
    if (Field(ml_ops, GETATTR) != Val_int(0)) {
	fs->fuse_ops.getattr = getattr_stub;
    }
    if (Field(ml_ops, SETATTR) != Val_int(0)) {
	fs->fuse_ops.setattr = setattr_stub;
    }
    if (Field(ml_ops, READ) != Val_int(0)) {
	fs->fuse_ops.read = read_stub;
    }
    if (Field(ml_ops, WRITE) != Val_int(0)) {
	fs->fuse_ops.write = write_stub;
    }
    if (Field(ml_ops, READDIR) != Val_int(0)) {
	fs->fuse_ops.readdir = readdir_stub;
    }
    if (Field(ml_ops, MKNOD) != Val_int(0)) {
	fs->fuse_ops.mknod = mknod_stub;
    }
    if (Field(ml_ops, STATFS) != Val_int(0)) {
	fs->fuse_ops.statfs = statfs_stub;
    }
    if (Field(ml_ops, READLINK) != Val_int(0)) {
	fs->fuse_ops.readlink = readlink_stub;
    }
    if (Field(ml_ops, MKDIR) != Val_int(0)) {
	fs->fuse_ops.mkdir = mkdir_stub;
    }
    if (Field(ml_ops, UNLINK) != Val_int(0)) {
	fs->fuse_ops.unlink = unlink_stub;
    }
    if (Field(ml_ops, RMDIR) != Val_int(0)) {
	fs->fuse_ops.rmdir = rmdir_stub;
    }
    if (Field(ml_ops, SYMLINK) != Val_int(0)) {
	fs->fuse_ops.symlink = symlink_stub;
    }
    if (Field(ml_ops, RENAME) != Val_int(0)) {
	fs->fuse_ops.rename = rename_stub;
    }
    if (Field(ml_ops, LINK) != Val_int(0)) {
	fs->fuse_ops.link = link_stub;
    }
    if (Field(ml_ops, OPENFILE) != Val_int(0)) {
	fs->fuse_ops.open = open_stub;
    }
    if (Field(ml_ops, FLUSH) != Val_int(0)) {
	fs->fuse_ops.flush = flush_stub;
    }
    if (Field(ml_ops, RELEASE) != Val_int(0)) {
	fs->fuse_ops.release = release_stub;
    }
    if (Field(ml_ops, FSYNC) != Val_int(0)) {
	fs->fuse_ops.fsync = fsync_stub;
    }
    if (Field(ml_ops, OPENDIR) != Val_int(0)) {
	fs->fuse_ops.opendir = opendir_stub;
    }
    if (Field(ml_ops, RELEASEDIR) != Val_int(0)) {
	fs->fuse_ops.releasedir = releasedir_stub;
    }
    if (Field(ml_ops, FSYNCDIR) != Val_int(0)) {
	fs->fuse_ops.fsyncdir = fsyncdir_stub;
    }
    if (Field(ml_ops, SETXATTR) != Val_int(0)) {
	fs->fuse_ops.setxattr = setxattr_stub;
    }
    if (Field(ml_ops, GETXATTR) != Val_int(0)) {
	fs->fuse_ops.getxattr = getxattr_stub;
    }
    if (Field(ml_ops, LISTXATTR) != Val_int(0)) {
	fs->fuse_ops.listxattr = listxattr_stub;
    }
    if (Field(ml_ops, REMOVEXATTR) != Val_int(0)) {
	fs->fuse_ops.removexattr = removexattr_stub;
    }
    if (Field(ml_ops, ACCESS) != Val_int(0)) {
	fs->fuse_ops.access = access_stub;
    }
    if (Field(ml_ops, CREATE) != Val_int(0)) {
	fs->fuse_ops.create = create_stub;
    }
    if (Field(ml_ops, GETLK) != Val_int(0)) {
	fs->fuse_ops.getlk = getlk_stub;
    }
    if (Field(ml_ops, SETLK) != Val_int(0)) {
	fs->fuse_ops.setlk = setlk_stub;
    }
    if (Field(ml_ops, BMAP) != Val_int(0)) {
	fs->fuse_ops.bmap = bmap_stub;
    }

    enter_blocking_section();
    // Mount filesystem
    fs->ch      = fuse_mount(String_val(ml_mountpoint), &fuse_args);
    if (fs->ch == NULL) goto out2;
    fs->se      = fuse_lowlevel_new(&fuse_args, &fs->fuse_ops, sizeof(fs->fuse_ops), (void*)fs);
    if (fs->se == NULL) goto out2;
    if (fuse_set_signal_handlers(fs->se) == -1) goto out2;
    fuse_session_add_chan(fs->se, fs->ch);
    leave_blocking_section();

    // Free fuse args
    fuse_opt_free_args(&fuse_args);

    // All done, return filesystem
    CAMLreturn(ml_fs);

out2:
    leave_blocking_section();
    umount_common(fs);
out:
    // Free fuse args
    fuse_opt_free_args(&fuse_args);

    // FIXME: throw exception
    fprintf(stderr, "  mounting failed\n");
    fflush(stderr);
    exit(1);
}

CAMLprim value caml_fuse_fd_stub(value ml_fs) {
    CAMLparam1(ml_fs);
    Filesystem *fs = *(Filesystem**)Data_custom_val(ml_fs);
    // FIXME: throw exception
    assert(alive(fs));
    fprintf(stderr, "fuse FD = %d\n", fuse_chan_fd(fs->ch));
    CAMLreturn(Val_int(fuse_chan_fd(fs->ch)));
}

CAMLprim value caml_fuse_session_exited(value ml_fs) {
    CAMLparam1(ml_fs);
    Filesystem *fs = *(Filesystem**)Data_custom_val(ml_fs);
    if (!alive(fs) || fs->se == NULL || fuse_session_exited(fs->se) != 0) {
	CAMLreturn(Val_true);
    } else {
	CAMLreturn(Val_false);
    }
}

CAMLprim value caml_fuse_process_stub(value ml_fs) {
    CAMLparam1(ml_fs);
    Filesystem *fs = *(Filesystem**)Data_custom_val(ml_fs);
    // FIXME: throw exception
    assert(alive(fs));
    static size_t size;
    if (buffer == NULL) {
	size = fuse_chan_bufsize(fs->ch);
	buffer_size = size + PAGE_SIZE;
	buffer = (char*)malloc(buffer_size);
    }
    // FIXME: throw exception
    assert(buffer != NULL);
    char *buf = (char*)(((intptr_t)buffer + HEADER_SIZE + PAGE_SIZE - 1) & ~(PAGE_SIZE - 1)) - HEADER_SIZE;
    buffer2 = buf;
    int res;
    fprintf(stderr, "# caml_fuse_process()\n");
    fflush(stderr);

    enter_blocking_section();
    if (fuse_session_exited(fs->se) != 0) {
	fprintf(stderr, "  session exited\n");
	fflush(stderr);
	umount_common(fs);
	free(buffer);
	buffer = NULL;
	leave_blocking_section();
	CAMLreturn(Val_unit);
    }
    fprintf(stderr, "  fuse_chan_recv\n");
    fflush(stderr);
    struct fuse_chan *tmpch = fs->ch;
    res = fuse_chan_recv(&tmpch, buf, size);
    fprintf(stderr, "  res = %d\n", res);
    fflush(stderr);
    if (res > 0) {
	fprintf(stderr, "   fuse_session_process\n");
	fflush(stderr);
	fuse_session_process(fs->se, buf, res, tmpch);
	fprintf(stderr, "     done\n");
	fflush(stderr);
    } else if (fuse_session_exited(fs->se) != 0) {
	fprintf(stderr, "  session exited\n");
	fflush(stderr);
	umount_common(fs);
	free(buffer);
	buffer = NULL;
    }
    assert(res >= 0);

    leave_blocking_section();
    CAMLreturn(Val_unit);
}

// external fuse_reply_err : fuse_req -> int -> int = "caml_fuse_reply_err_stub"
value caml_fuse_reply_err_stub(value req, value error) {
    int res;
    CAMLparam2(req, error);

    int err = Int_val(error);
    assert(err >= 0);
    assert((unsigned)err < sizeof(errors) / sizeof(errors[0]));
    res = fuse_reply_err((fuse_req_t)Field(req, 0), errors[err]);

    CAMLreturn(Val_int(res));
}

// external fuse_reply_none : fuse_req -> int = "caml_fuse_reply_none_stub"
value caml_fuse_reply_none_stub(value req) {
    CAMLparam1(req);

    fuse_reply_none((fuse_req_t)Field(req, 0));

    CAMLreturn(Val_int(0));
}

// external fuse_reply_entry : fuse_req -> entry -> int = "caml_fuse_reply_entry_stub"
value caml_fuse_reply_entry_stub(value req, value entry) {
    int res;
    CAMLparam2(req, entry);

    struct fuse_entry_param fuse_entry = {
	.ino = Int64_val(Field(entry, 0)),
	.generation = Int64_val(Field(entry, 1)),
	.attr_timeout = Double_val(Field(entry, 3)),
	.entry_timeout = Double_val(Field(entry, 4)),
    };
    stats_ml_to_c(Field(entry, 2), &fuse_entry.attr);

    res = fuse_reply_entry((fuse_req_t)Field(req, 0), &fuse_entry);

    CAMLreturn(Val_int(res));
}

// external fuse_reply_create : fuse_req -> entry -> int = "caml_fuse_reply_create_stub"
value caml_fuse_reply_create_stub(value req, value entry, value file_info) {
    int res;
    CAMLparam3(req, entry, file_info);

    struct fuse_entry_param fuse_entry = {
	.ino = Int64_val(Field(entry, 0)),
	.generation = Int64_val(Field(entry, 1)),
	.attr_timeout = Double_val(Field(entry, 3)),
	.entry_timeout = Double_val(Field(entry, 4)),
    };
    stats_ml_to_c(Field(entry, 2), &fuse_entry.attr);

    res = fuse_reply_create((fuse_req_t)Field(req, 0), &fuse_entry,
			    (const struct fuse_file_info*)file_info);

    CAMLreturn(Val_int(res));
}

// external fuse_reply_attr : fuse_req -> stats -> float -> int = "caml_fuse_reply_attr_stub"
value caml_fuse_reply_attr_stub(value req, value ml_stats, value timeout) {
    int res;
    CAMLparam3(req, ml_stats, timeout);

    struct stat stats;
    stats_ml_to_c(ml_stats, &stats);
    res = fuse_reply_attr((fuse_req_t)Field(req, 0), &stats, Double_val(timeout));

    printf("# fuse_reply_attr_stub(): ino = %lu, mode = 0%o, nlink = %lu, uid = %d, gid = %d, size = %lu, blocks = %lu\n", stats.st_ino, stats.st_mode, stats.st_nlink, stats.st_uid, stats.st_gid, stats.st_size, stats.st_blocks);
    CAMLreturn(Val_int(res));
}

// external fuse_reply_readlink : fuse_req -> string -> int = "caml_fuse_reply_readlink_stub"
value caml_fuse_reply_readlink_stub(value ml_req, value ml_link) {
    int res;
    CAMLparam2(ml_req, ml_link);

    res = fuse_reply_readlink((fuse_req_t)Field(ml_req, 0), (const char*)String_val(ml_link));

    CAMLreturn(Val_int(res));
}

// external fuse_reply_open : fuse_req -> file_info -> int = "caml_fuse_reply_open_stub"
value caml_fuse_reply_open_stub(value ml_req, value ml_file_info) {
    int res;
    CAMLparam2(ml_req, ml_file_info);

    res = fuse_reply_open((fuse_req_t)Field(ml_req, 0), (const struct fuse_file_info*)Field(ml_file_info, 0));

    CAMLreturn(Val_int(res));
}

// external fuse_reply_write : fuse_req -> int -> int = "caml_fuse_reply_write_stub"
value caml_fuse_reply_write_stub(value ml_req, value ml_size) {
    int res;
    CAMLparam2(ml_req, ml_size);

    res = fuse_reply_write((fuse_req_t)Field(ml_req, 0), Int_val(ml_size));

    CAMLreturn(Val_int(res));
}

// external fuse_reply_buf : fuse_req -> buf_t -> int = "caml_fuse_reply_buf_stub"
value caml_fuse_reply_buf_stub(value ml_req, value ml_buf) {
    int res;
    CAMLparam2(ml_req, ml_buf);

    res = fuse_reply_buf((fuse_req_t)Field(ml_req, 0), Data_bigarray_val(ml_buf), Bigarray_val(ml_buf)->dim[0]);

    CAMLreturn(Val_int(res));
}

// external fuse_reply_strbuf : fuse_req -> string -> int = "caml_fuse_reply_strbuf_stub"
value caml_fuse_reply_strbuf_stub(value ml_req, value ml_buf) {
    int res;
    CAMLparam2(ml_req, ml_buf);

    res = fuse_reply_buf((fuse_req_t)Field(ml_req, 0), String_val(ml_buf), caml_string_length(ml_buf));

    CAMLreturn(Val_int(res));
}

// external fuse_reply_striov : fuse_req -> string array -> int = "caml_fuse_reply_striov_stub"
value caml_fuse_reply_iov_stub(value ml_req, value ml_bufs) {
    int res;
    CAMLparam2(ml_req, ml_bufs);
    fprintf(stderr, "caml_fuse_reply_iov_stub() is incomplete\n");
//FIXME: create struct iovec
// int fuse_reply_iov(fuse_req_t req, const struct iovec *iov, int count);
//    res = fuse_reply_iov((fuse_req_t)Field(ml_req, 0), Sting_val(ml_buf), caml_string_length(ml_buf));
    assert(0);

    CAMLreturn(Val_int(res));
}

// external fuse_reply_striov : fuse_req -> string array -> int = "caml_fuse_reply_striov_stub"
value caml_fuse_reply_striov_stub(value ml_req, value ml_bufs) {
    int res;
    CAMLparam2(ml_req, ml_bufs);
    fprintf(stderr, "caml_fuse_reply_striov_stub() is incomplete\n");
//FIXME: create struct iovec
// int fuse_reply_iov(fuse_req_t req, const struct iovec *iov, int count);
//    res = fuse_reply_iov((fuse_req_t)Field(ml_req, 0), Sting_val(ml_buf), caml_string_length(ml_buf));
    assert(0);

    CAMLreturn(Val_int(res));
}

// external fuse_reply_statfs : fuse_req -> statfs -> int = "caml_fuse_reply_statfs_stub"
value caml_fuse_reply_statfs_stub(value ml_req, value ml_statfs) {
    int res;
    CAMLparam2(ml_req, ml_statfs);
    struct statvfs stbuf;
    memset(&stbuf, 0, sizeof(stbuf));
    stbuf.f_bsize   = Int64_val(Field(ml_statfs, 0));    /* file system block size */
    stbuf.f_frsize  = Int64_val(Field(ml_statfs, 1));   /* fragment size */
    stbuf.f_blocks  = Int64_val(Field(ml_statfs, 2));   /* size of fs in f_frsize units */
    stbuf.f_bfree   = Int64_val(Field(ml_statfs, 3));    /* # free blocks */
    stbuf.f_bavail  = Int64_val(Field(ml_statfs, 4));   /* # free blocks for non-root */
    stbuf.f_files   = Int64_val(Field(ml_statfs, 5));    /* # inodes */
    stbuf.f_ffree   = Int64_val(Field(ml_statfs, 6));    /* # free inodes */
    stbuf.f_favail  = Int64_val(Field(ml_statfs, 7));   /* # free inodes for non-root */
    stbuf.f_fsid    = Int64_val(Field(ml_statfs, 8));     /* file system ID */
    stbuf.f_flag    = Int64_val(Field(ml_statfs, 9));     /* mount flags */
    stbuf.f_namemax = Int64_val(Field(ml_statfs, 10));  /* maximum filename length */

    printf("# reply_statfs: f_blocks = %ld, f_bfree = %ld\n", stbuf.f_blocks, stbuf.f_bfree);
    res = fuse_reply_statfs((fuse_req_t)Field(ml_req, 0), &stbuf);

    CAMLreturn(Val_int(res));
}

// external fuse_reply_xattr : fuse_req -> int -> int = "caml_fuse_reply_xattr_stub"
value caml_fuse_reply_xattr_stub(value ml_req, value ml_count) {
    int res;
    CAMLparam2(ml_req, ml_count);

    res = fuse_reply_xattr((fuse_req_t)Field(ml_req, 0), Int_val(ml_count));

    CAMLreturn(Val_int(res));
}

// external fuse_reply_lock : fuse_req -> lock_info -> int = "caml_fuse_reply_lock_stub"
value caml_fuse_reply_lock_stub(value ml_req, value ml_lock_info) {
    int res;
    CAMLparam2(ml_req, ml_lock_info);

    res = fuse_reply_lock((fuse_req_t)Field(ml_req, 0), (struct flock*)Field(ml_lock_info, 0));

    CAMLreturn(Val_int(res));
}

// external fuse_reply_bmap : fuse_req -> int64 -> int = "caml_fuse_reply_bmap_stub"
value caml_fuse_reply_bmap_stub(value ml_req, value ml_idx) {
    int res;
    CAMLparam2(ml_req, ml_idx);

    res = fuse_reply_bmap((fuse_req_t)Field(ml_req, 0), Int64_val(ml_idx));

    CAMLreturn(Val_int(res));
}

//  external add_direntry : fuse_req -> string -> int -> (string * stat * int64) -> int = "caml_fuse_add_direntry_stub"
value caml_fuse_add_direntry_stub(value ml_req, value ml_buf, value ml_pos, value ml_entry) {
    CAMLparam4(ml_req, ml_buf, ml_pos, ml_entry);

    fprintf(stderr, "#### caml_fuse_add_direntry_stub()\n");
    fflush(stderr);

    fuse_req_t req = (fuse_req_t)Field(ml_req, 0);
    char *buf = String_val(ml_buf);
    size_t pos = Int_val(ml_pos);
    size_t len = caml_string_length(ml_buf) - pos;
    const char *name = String_val(Field(ml_entry, 0));
    struct stat stats;
    uint64_t off = Int64_val(Field(ml_entry, 2));
    buf += pos;
    stats_ml_to_c(Field(ml_entry, 1), &stats);

    len = fuse_add_direntry(req, buf, len, name, &stats, off);

    CAMLreturn(Val_int(pos + len));
}

// external context : fuse_req -> context = "caml_fuse_context_stub
value caml_fuse_context_stub(value ml_req) {
    CAMLparam1(ml_req);
    CAMLlocal1(ml_ctx);

    fprintf(stderr, "#### caml_fuse_context_stub()\n");
    fflush(stderr);

    fuse_req_t req = (fuse_req_t)Field(ml_req, 0);
    const struct fuse_ctx *ctx = fuse_req_ctx(req);
    ml_ctx = caml_alloc(3, 0);
    Store_field(ml_ctx, 0, Val_int(ctx->uid));
    Store_field(ml_ctx, 1, Val_int(ctx->gid));
    Store_field(ml_ctx, 2, Val_int(ctx->pid));

    CAMLreturn(ml_ctx);
}

// external unsafe_substr : t -> int -> int -> string = "caml_fuse_buf_unsafe_substr_stub"
value caml_fuse_buf_unsafe_substr_stub(value ml_buf, value ml_off, value ml_len) {
    CAMLparam3(ml_buf, ml_off, ml_len);
    CAMLlocal1(ml_str);
    
    const char *src = Data_bigarray_val(ml_buf);
    size_t off = Int_val(ml_off);
    size_t len = Int_val(ml_len);
    ml_str = caml_alloc_string(len);
    char *dst = String_val(ml_str);
    memcpy(dst, &src[off], len);

    CAMLreturn(ml_str);
}
