(* fuse.mli: interface for libfuse-ocaml
 * Copyright (C) 2009 Goswin von Brederlow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Under Debian a copy can be found in /usr/share/common-licenses/LGPL-3.
 *)

(** libfuse-ocaml - Filesystem in userspace interface for ocaml

    This module implements the libfuse bindings that interface with the
    kernel to implement filesystems in userspace.

    {e Version 0.0.0 - goswin-v-b\@web.de }

    {1:top  }
*)

type error =
    SUCCESS
  | ENOENT
  | EIO
  | ENOTDIR
  | EISDIR
  | EPERM
  | ENOTEMPTY
  | ENOSPC
  (** Kinds of errors libfuse-ocaml knows *)

type fuse_req
  (** Abstract type for a fuse request *)

type inode = int64
    (** inode type *)

type pid_t = int
    (** process id type *)

module Buf : sig
  type t = (int, Bigarray.int8_unsigned_elt, Bigarray.c_layout) Bigarray.Array1.t
    (** buffer type *)
  external unsafe_substr : t -> int -> int -> string = "caml_fuse_buf_unsafe_substr_stub"
    (** unsafe extract string from buffer *)
  val substr : t -> int -> int -> string
    (** extract string from buffer *)
  val str : t -> string
    (** convert buffer to string *)
end

type stats = {
  st_dev : int;
  st_ino : int64;
  st_mode : int;
  st_nlink : int;
  st_uid : int;
  st_gid : int;
  st_rdev : int;
  st_size : int64;
  st_blksize : int64;
  st_blocks : int64;
  st_atime : float;
  st_mtime : float;
  st_ctime : float;
}
    (** file statistics for getattr operation *)

type entry = {
  e_inode : inode;
  e_generation : int64;
  e_stats : stats;
  e_stats_timeout : float;
  e_entry_timeout : float;
}
    (** result of lookup operations *)

type open_flags = int
type 'a file_info = {
  fi_flags : open_flags;
  fi_writepage : bool;
  fi_direct_io : bool;
  fi_keep_cache : bool;
  fi_flush : bool;
  fi_fh : 'a option;
  fi_lock_owner : int64;
}
type context = {
  ctx_uid : int;
  ctx_gid : int;
  ctx_pid : pid_t;
}
type set_attr_flags = {
  set_attr_mode : bool;
  set_attr_uid : bool;
  set_attr_gid : bool;
  set_attr_size : bool;
  set_attr_atime : bool;
  set_attr_mtime : bool;
  set_attr_fh : bool;
  set_attr_atime_now : bool;
  set_attr_mtime_now : bool;
  set_attr_lockowner : bool;
}
type mode = int
type dev = int
type statfs = {
  f_bsize : int64;
  f_frsize : int64;
  f_blocks : int64;
  f_bfree : int64;
  f_bavail : int64;
  f_files : int64;
  f_ffree : int64;
  f_favail : int64;
  f_fsid : int64;
  f_flag : int64;
  f_namemax : int64;
}
type flock = {
  l_type : int;
  l_whence : int;
  l_start : int64;
  l_len : int64;
  l_pid : pid_t;
}
external fuse_reply_err : fuse_req -> error -> unit
  = "caml_fuse_reply_err_stub"
external fuse_reply_none : fuse_req -> unit = "caml_fuse_reply_none_stub"
external fuse_reply_entry : fuse_req -> entry -> unit
  = "caml_fuse_reply_entry_stub"
external fuse_reply_create : fuse_req -> entry -> 'a file_info -> unit
  = "caml_fuse_reply_create_stub"
external fuse_reply_attr : fuse_req -> stats -> float -> unit
  = "caml_fuse_reply_attr_stub"
external fuse_reply_readlink : fuse_req -> string -> unit
  = "caml_fuse_reply_readlink_stub"
external fuse_reply_open : fuse_req -> 'a file_info -> unit
  = "caml_fuse_reply_open_stub"
external fuse_reply_write : fuse_req -> int -> unit
  = "caml_fuse_reply_write_stub"
external fuse_reply_buf : fuse_req -> Buf.t -> unit
  = "caml_fuse_reply_buf_stub"
external fuse_reply_iov : fuse_req -> Buf.t array -> unit
  = "caml_fuse_reply_iov_stub"
external fuse_reply_strbuf : fuse_req -> string -> unit
  = "caml_fuse_reply_strbuf_stub"
external fuse_reply_striov : fuse_req -> string array -> unit
  = "caml_fuse_reply_striov_stub"
external fuse_reply_statfs : fuse_req -> statfs -> unit
  = "caml_fuse_reply_statfs_stub"
external fuse_reply_xattr : fuse_req -> int -> unit
  = "caml_fuse_reply_xattr_stub"
external fuse_reply_lock : fuse_req -> flock -> unit
  = "caml_fuse_reply_lock_stub"
external fuse_reply_bmap : fuse_req -> int64 -> unit
  = "caml_fuse_reply_bmap_stub"
type 'a reply =
    [ `Attr of stats * float
    | `Bmap of int64
    | `Buf of Buf.t
    | `StrBuf of string
    | `Create of entry * 'a file_info
    | `Entry of entry
    | `Err of error
    | `Iov of Buf.t array
    | `StrIov of string array
    | `Lock of flock
    | `None
    | `Open of 'a file_info
    | `Readlink of string
    | `Statfs of statfs
    | `Write of int
    | `Xattr of int ]
val reply :
  fuse_req ->
  [< `Attr of stats * float
   | `Bmap of int64
   | `Buf of Buf.t
   | `StrBuf of string
   | `Create of entry * 'a file_info
   | `Entry of entry
   | `Err of error
   | `Iov of Buf.t array
   | `StrIov of string array
   | `Lock of flock
   | `None
   | `Open of 'b file_info
   | `Readlink of string
   | `Statfs of statfs
   | `Write of int
   | `Xattr of int ] ->
  unit
type lookup_res = [ `Entry of entry | `Err of error ]
val reply_lookup : fuse_req -> lookup_res -> unit
type forget_res = [ `None ]
val reply_forget : fuse_req -> forget_res -> unit
type getattr_res = [ `Attr of stats * float | `Err of error ]
val reply_getattr : fuse_req -> getattr_res -> unit
type setattr_res = [ `Attr of stats * float | `Err of error ]
val reply_setattr : fuse_req -> setattr_res -> unit
type readlink_res = [ `Err of error | `Readlink of string ]
val reply_readlink : fuse_req -> readlink_res -> unit
type mknod_res = [ `Entry of entry | `Err of error ]
val reply_mknod : fuse_req -> mknod_res -> unit
type mkdir_res = [ `Entry of entry | `Err of error ]
val reply_mkdir : fuse_req -> mkdir_res -> unit
type unlink_res = [ `Err of error ]
val reply_unlink : fuse_req -> unlink_res -> unit
type rmdir_res = [ `Err of error ]
val reply_rmdir : fuse_req -> rmdir_res -> unit
type symlink_res = [ `Entry of entry | `Err of error ]
val reply_symlink : fuse_req -> symlink_res -> unit
type rename_res = [ `Err of error ]
val reply_rename : fuse_req -> rename_res -> unit
type link_res = [ `Entry of entry | `Err of error ]
val reply_link : fuse_req -> link_res -> unit
type 'a open_res = [ `Err of error | `Open of 'a file_info ]
val reply_open : fuse_req -> 'a open_res -> unit
type read_res = [ `Buf of Buf.t | `StrBuf of string | `Err of error | `Iov of Buf.t array | `StrIov of string array ]
val reply_read : fuse_req -> read_res -> unit
type write_res = [ `Err of error | `Write of int ]
val reply_write : fuse_req -> write_res -> unit
type flush_res = [ `Err of error ]
val reply_flush : fuse_req -> flush_res -> unit
type release_res = [ `Err of error ]
val reply_release : fuse_req -> release_res -> unit
type fsync_res = [ `Err of error ]
val reply_fsync : fuse_req -> fsync_res -> unit
type 'a opendir_res = [ `Err of error | `Open of 'a file_info ]
val reply_opendir : fuse_req -> 'a opendir_res -> unit
type readdir_res = [ `Buf of Buf.t | `StrBuf of string | `Err of error | `Iov of Buf.t array | `StrIov of string array ]
val reply_readdir : fuse_req -> readdir_res -> unit
type releasedir_res = [ `Err of error ]
val reply_releasedir : fuse_req -> releasedir_res -> unit
type fsyncdir_res = [ `Err of error ]
val reply_fsyncdir : fuse_req -> fsyncdir_res -> unit
type statfs_res = [ `Err of error | `Statfs of statfs ]
val reply_statfs : fuse_req -> statfs_res -> unit
type setxattr_res = [ `Err of error ]
val reply_setxattr : fuse_req -> setxattr_res -> unit
type getxattr_res =
    [ `Buf of Buf.t | `StrBuf of string | `Err of error | `Iov of Buf.t array | `StrIov of string array | `Xattr of int ]
val reply_getxattr : fuse_req -> getxattr_res -> unit
type listxattr_res =
    [ `Buf of Buf.t | `StrBuf of string | `Err of error | `Iov of Buf.t array | `StrIov of string array | `Xattr of int ]
val reply_listxattr : fuse_req -> listxattr_res -> unit
type removexattr_res = [ `Err of error ]
val reply_removexattr : fuse_req -> removexattr_res -> unit
type access_res = [ `Err of error ]
val reply_access : fuse_req -> access_res -> unit
type 'a create_res = [ `Create of entry * 'a file_info | `Err of error ]
val reply_create : fuse_req -> 'a create_res -> unit
type getlk_res = [ `Err of error | `Lock of flock ]
val reply_getlk : fuse_req -> getlk_res -> unit
type setlk_res = [ `Err of error ]
val reply_setlk : fuse_req -> setlk_res -> unit
type bmap_res = [ `Bmap of int64 | `Err of error ]
val reply_bmap : fuse_req -> bmap_res -> unit

type 'a ops = {
  init : unit -> unit;
  destroy : unit -> unit;
  lookup : fuse_req -> inode -> string -> unit;
  forget : fuse_req -> inode -> int64 -> unit;
  getattr : fuse_req -> inode -> unit;
  setattr :
    fuse_req ->
    inode -> stats -> set_attr_flags -> 'a file_info option -> unit;
  readlink : fuse_req -> inode -> unit;
  mknod : fuse_req -> inode -> string -> mode -> dev -> unit;
  mkdir : fuse_req -> inode -> string -> mode -> unit;
  unlink : fuse_req -> inode -> string -> unit;
  rmdir : fuse_req -> inode -> string -> unit;
  symlink : fuse_req -> string -> inode -> string -> unit;
  rename : fuse_req -> inode -> string -> inode -> string -> unit;
  link : fuse_req -> inode -> inode -> string -> unit;
  openfile : fuse_req -> inode -> 'a file_info -> unit;
  read : fuse_req -> inode -> int -> int64 -> 'a file_info -> unit;
  write : fuse_req -> inode -> Buf.t -> int64 -> 'a file_info -> unit;
  flush : fuse_req -> inode -> 'a file_info -> unit;
  release : fuse_req -> inode -> 'a file_info -> unit;
  fsync : fuse_req -> inode -> bool -> 'a file_info -> unit;
  opendir : fuse_req -> inode -> 'a file_info -> unit;
  readdir : fuse_req -> inode -> int -> int64 -> 'a file_info -> unit;
  releasedir : fuse_req -> inode -> 'a file_info -> unit;
  fsyncdir : fuse_req -> inode -> bool -> 'a file_info -> unit;
  statfs : fuse_req -> inode -> unit;
  setxattr : fuse_req -> inode -> string -> string -> int -> unit;
  getxattr : fuse_req -> inode -> string -> int -> unit;
  listxattr : fuse_req -> inode -> int -> unit;
  removexattr : fuse_req -> inode -> string -> unit;
  access : fuse_req -> inode -> int -> unit;
  create : fuse_req -> inode -> string -> mode -> 'a file_info -> unit;
  getlk : fuse_req -> inode -> 'a file_info -> flock -> unit;
  setlk : fuse_req -> inode -> 'a file_info -> flock -> bool -> unit;
  bmap : fuse_req -> inode -> int64 -> int64 -> unit;
}
    (** filesystem operation
	init    - start the filesystem
	destroy - shutdown filesystem
	lookup  - lookup inode number by directory inode and filename
	getattr - get stats for an inode
    *)

val default_ops : unit -> 'a ops

val root_inode : int64
val s_IFDIR : int
val s_IFREG : int
val s_IFMASK : int
val s_ISDIR : int -> bool
val s_ISREG : int -> bool
type filesystem
  (** abstract type representing a fuse filesystem *)

(* val mount : string -> string list -> fuse_ops -> filesystem *)
val make : string -> string list -> 'a ops -> filesystem
  (** make mountpoint options operations wait_fn
      Create a userspace filesystem <ops> on <mountpoint> with
      <option>. <wait_fn> will be called between requests and may
      block until the file descriptor is ready for reading.
  *)

val umount : filesystem -> unit
  (** umount a fuse filesystem *)

val fd : filesystem -> Unix.file_descr
  (** get file descriptor of filesystem *)

val session_exited : filesystem -> bool
  (** true if the filesystem session exited *)

val process : filesystem -> unit
  (** process a pending filesystem operation
      will block till an operation is pending
  *)

val context : fuse_req -> context
  (** return additional fuse context for a request *)

val root_inode : int64
  (** inode number for the root inode *)

val s_IFDIR : int
  (** constant to declare a directory in st_mode *)

val s_IFREG : int
  (** constant to declare a file in st_mode *)

val s_IFMASK : int
  (** constant to mask file type in st_mode *)

val s_ISDIR : int -> bool
  (** test for directory in st_mode *)

val s_ISREG : int -> bool
  (** test for file in st_mode *)

val add_direntry : fuse_req -> string -> int -> string * stats * int64 -> int

