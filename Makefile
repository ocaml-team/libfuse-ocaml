.PHONY: all
all:
	@cd lib && $(MAKE) byte-code-library native-code-library

.PHONY: opt
opt:
	@cd lib && $(MAKE) native-code-library

.PHONY: opt
byte:
	@cd lib && $(MAKE) byte-code-library

.PHONY:	doc
doc:
	@cd lib && $(MAKE) $@
	ln -sf lib/doc

.PHONY:	install
install:
	@cd lib && $(MAKE) $@

.PHONY:	uninstall
uninstall:
	@cd lib && $(MAKE) $@

.PHONY:	clean
clean:
	@cd lib && $(MAKE) clean
	@cd examples && $(MAKE) clean
	@rm -f doc

distclean: clean

test:
	@cd examples && $(MAKE) test
