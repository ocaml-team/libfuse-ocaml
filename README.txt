...............................................................................
libfuse-ocaml - OCaml bindings for libfuse (filesystem in userspace library)
...............................................................................

libfuse-ocaml are OCaml bindings enabling ocaml programs to implement
filesystems in userspace.

libfuse-ocaml is made of a single, independent, module and distributed under
the GNU Lesser General Public License.

Vcs-Git: git://git.debian.org/pkg-ocaml-maint/packages/libfuse-ocaml.git
Vcs-Browser: http://git.debian.org/git/pkg-ocaml-maint/packages/libfuse-ocaml.git
Contact : Goswin von Brederlow <goswin-v-b@web.de>

...............................................................................

You can compile the example using:

ocamlopt -I $(ocamlopt -where)/fuse -o fs fuse.cmxa examples/fs.ml
